class RemovePasswordColumnBecuseBcryptGemDoesNotWantIt < ActiveRecord::Migration[5.1]
  def change
    remove_column :companies, :password, :string
  end
end
