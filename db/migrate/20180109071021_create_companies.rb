class CreateCompanies < ActiveRecord::Migration[5.1]
  def change
    create_table :companies do |t|
      t.string :name, index: true
      t.string :email, index: true
      t.string :password, limit: 50

      t.timestamps
    end
  end
end
