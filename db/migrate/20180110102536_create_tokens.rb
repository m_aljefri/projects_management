class CreateTokens < ActiveRecord::Migration[5.1]
  def change
    create_table :tokens do |t|
      t.integer :company_id, index: true, foreign_key: true
      t.string :token

      t.timestamps
    end
  end
end
