class CreateEmployees < ActiveRecord::Migration[5.1]
  def change
    create_table :employees do |t|
      t.string :name
      t.date :joining_date
      t.date :birth_date
      t.belongs_to :company, foreign_key: true, index: true

      t.timestamps
    end
  end
end
