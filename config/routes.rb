Rails.application.routes.draw do
    concern :api_routes do
    namespace :company, path: '/' do
      # Company crud opreations
      resource :companies do 

        namespace :employees , path: '/' do
          # employess method endpoints
          resources :employees do 
            resources :projects_of_employees, only: [:create, :index], path: 'projects'
          end       
        end
        namespace :projects, path: '/' do  
          # projects method endpoints
          resources :projects do
            resources :employees_of_projects, only: [:create, :index], path: 'employees'
            resources :tasks #Tasks of project
          end
        end

      end
      # Password method endpoints
      resource :passwords, only: :create do
        collection do 
          put "/:reset_password", to: 'passwords#update'
        end
      end      
      # Session managing login/logout
      resource :session, path: '/company/session', only: [:create, :destroy]
      


    end
  end
  scope ':locale', locale: /#{I18n.available_locales.join('|')}/ do
    namespace :v1, constraints: { format: 'json' } do
      concerns :api_routes
    end
  end
end
