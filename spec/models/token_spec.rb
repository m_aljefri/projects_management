require 'rails_helper'

RSpec.describe Token, type: :model do
  it { should have_secure_token }

  it { should validate_presence_of :token }
  it { should belong_to :company }
end
