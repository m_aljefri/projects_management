require 'rails_helper'

RSpec.describe Employee, type: :model do
  it { should belong_to :company }
  it { should have_many :tasks }
end
