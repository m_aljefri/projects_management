require 'rails_helper'

RSpec.describe Company, type: :model do
 
  it { should have_secure_password }
  it { should have_many :employees }
  it { should have_many :projects  }

  it { should validate_presence_of(:password) }
  it { should validate_presence_of(:email) }
  it { should validate_uniqueness_of(:name) }
  it { should validate_uniqueness_of(:email) }
  it { should validate_presence_of(:name )}
  it { should validate_confirmation_of(:password)}
end
