FactoryBot.define do
  factory :task do
    name "MyString"
    description "MyText"
    assignee_id 1
    status "MyString"
  end
end
