module ControllerHelper 
    #------------------------Token Authorization----------------------#
    def authenticated
        @token = request.headers['Authorization']
        token = Token.find_by_token @token if @token
        if token
            @current_company = token.company
        else
          render_invalid error: 'Unauthorized user'
        end
    end
    def current_company
        @current_company
    end
    def get_token
        @token
    end

    #------------------------------------------------------------------#



    #----------------------host link in mailer-------------------------#
    def make_action_mailer_use_request_host_and_protocol
        ActionMailer::Base.default_url_options[:protocol] = request.protocol
        ActionMailer::Base.default_url_options[:host] = request.host_with_port
    end
    #------------------------------------------------------------------#



    #------------------------sessions methods--------------------------#
    def valid_login? email_name
        @company = Company.where("email = ? OR user_name = ?",email_name,email_name).first
    end

    def generate_token
        loop do
            token = rand(36**8).to_s(36) 
            break token unless Company.where(reset_password: token).exists?
        end
    end
    #---------------------------------------------------------------------#


    #--------Ceate many to many relation between projects and employees-------#
    
    def employee_project_relation new_relation, my_collection, class_name
    old_relation = my_collection.map(&:id).map(&:to_s)
    related      = new_relation & old_relation              
    remove       = old_relation - related          
    add          = new_relation - related                   
    my_collection << class_name.find(add)        if add.present?
    my_collection.delete class_name.find(remove) if remove.present?
    end
    # examples describing method arguments:
    # my_collection maybe @employee.projects
    # new_relation maybe POSTED params as an array..['1','5']
    # class name is name of related class such Project , Employee and so on
    #-------------------------------------------------------------------------#
end