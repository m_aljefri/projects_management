module Response
    def render_valid data: nil, message: I18n.t('application.message') , status: :ok, pagination: nil
        render json: {
            success: true,
            message: message,
            data:  data,
            status: status,
          }, status: status
    end

    def render_invalid data: nil, error: I18n.t('application.error') ,status: :unprocessable_entity
        render json: {
            success: false,
            errore: error,
            data: data,
            status: status
          }, status: status
    end

    def serializer_collection objects ,serializer, scope: nil
        if scope then ActiveModel::Serializer::CollectionSerializer.new(objects,serializer: serializer, scope: scope)
        else ActiveModel::Serializer::CollectionSerializer.new(objects,serializer: serializer) end
    end
    def paginate_data objects
        {
            current_page: objects.current_page,
            next_page: objects.next_page,
            prev_page: objects.prev_page,
            total_pages: objects.total_pages,
            total_entries: objects.total_count,
            per_page: objects.count
        }
    end
    # never give up greate things take time
end