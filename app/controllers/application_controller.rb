class ApplicationController < ActionController::API
    include ExceptionHandler
    include Response
    include ControllerHelper


    before_action :authenticated, 
                  :make_action_mailer_use_request_host_and_protocol,
                  :set_locale

    
    def set_locale
      I18n.locale = params[:locale] || I18n.default_locale
    end
end
