module V1::Company::Projects
  class ProjectsController < ApplicationController
    before_action :set_project, only: [:show, :update, :destroy]
    def index
      @q = current_company.projects.ransack(name_cont: params[:name])
      projects = q.result.page(params[:page])
      render_valid data:  {projects: serializer_collection(projects, ProjectSerializer), pagination: paginate_data(projects)}
    end

    def show
      render_valid data: Show::ProjectSerializer.new(@project)
    end

    def create
      project = current_company.projects.create! project_params
      render_valid data: ProjectSerializer.new(project)
    end

    def update
      @project.update! project_params 
      render_valid data: ProjectSerializer.new(@project)  
    end

    def destroy
      render_valid message: 'Deleted successfully' if  @project.destroy
    end
    
    private

    def project_params
      params.permit(:name, :description)
    end

    def set_project
      @project = current_company.projects.find(params[:id]) if params[:id].present?
    end
  end
end