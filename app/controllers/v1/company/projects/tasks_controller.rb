module V1::Company::Projects
  class TasksController < ApplicationController
    before_action :set_project
    before_action :set_task, only: [:show, :update, :destroy]

    def index
      tasks = @project.tasks.all
      render_valid data: ActiveModel::Serializer::CollectionSerializer.new(tasks,serializer: TaskSerializer) 
    end

    def create
      task = @project.tasks.create! task_params
      render_valid data: TaskSerializer.new(task), 
                    message: 'Task is created successfully'
    end

    def update
      @task.update! task_params
      render_valid data: TaskSerializer.new(@task), 
      message: 'Task is upated successfully'
    end

    def destroy
      render_valid data: TaskSerializer.new(@task), 
      message: 'Task is deleted successfully' if @task.destroy
    end

    private

    def set_project
      @project = current_company.projects.find(params[:project_id])
    end
    def set_task
      @task = @project.tasks.find(params[:id])
    end
    def task_params
      params.permit(:name, :description, :status, :assignee_id)  
    end
  end
end
