module V1::Company::Projects
    class EmployeesOfProjectsController < ApplicationController
        before_action :set_project

        def index
            render_valid data: project_employees_serializer
        end

        def create
            employee_project_relation params[:employee_ids], @project.employees, Employee
            render_valid data: project_employees_serializer
        end

        private

        def project_employees_serializer
            @employees = current_company.employees
            serializer_collection(@employees, EmployeesProjectSerializer,scope: { 'employee_projects': @project.employees.map(&:id) })
        end


        def set_project
            @project = current_company.projects.find(params[:project_id]) if params[:project_id].present?
        end
    end
end