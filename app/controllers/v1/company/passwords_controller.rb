class V1::Company::PasswordsController < ApplicationController
    skip_before_action :authenticated, only: [:create, :update]

    # POST /v1/passwords
    def create
      company = ::Company.find_by_email params[:email]  if params[:email].present?
      company.update! reset_password: generate_token     if company
    end

    # PUT /v1/passwords/reset/:reset_password
    def update
      company = ::Company.find_by_reset_password params[:reset_password]
      company.update! password: params[:password], reset_password: nil if company 
    end
end