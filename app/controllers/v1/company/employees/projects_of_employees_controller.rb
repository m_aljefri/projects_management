module V1::Company::Employees
  class ProjectsOfEmployeesController < ApplicationController
    before_action :set_employee
    
    def index
      render_valid data: employee_projects_serializer 
    end

    def create  
      employee_project_relation params[:project_ids], @employee.projects, Project
      render_valid data: employee_projects_serializer
    end

    private
    
    def set_employee
      @employee = current_company.employees.find(params[:employee_id]) if params[:employee_id].present?
    end


    # Don't repeate yourself DRY
    def employee_projects_serializer
      @projects = current_company.projects
      serializer_collection(@projects, 
      EmployeesProjectSerializer,
      scope: {'employee_projects': @employee.projects.map(&:id)})
    end
  end
end