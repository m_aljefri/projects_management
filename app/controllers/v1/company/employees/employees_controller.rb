module V1::Company::Employees
  class EmployeesController < ApplicationController
    before_action :set_employee, only: [:show, :update, :destroy]

    # GET v1/companies/employees
    def index
      @q = current_company.employees.ransack(name_cont: params[:name])
      employees = @q.result.page(params[:page])
      render_valid data: {employees: serializer_collection(employees,EmployeeSerializer), pagination: paginate_data(employees)}
    end

    # GET v1/companies/employees/:id
    def show
      render_valid data:  Show::EmployeeSerializer.new(@employee) 
    end

    # POST v1/companies/employees
    def create
      employee = current_company.employees.create! employee_params
      render_valid status: :created
    end

    # PUT v1/companies/employees/:id
    def update
      @employee.update! employee_params 
      render_valid data: ::EmployeeSerializer.new(@employee)      
    end

    # DELETE v1/companies/employees
    def destroy
      render_valid message: 'Employee deleted successfully' if @employee.destroy
    end


    private
    
    
    def set_employee
      @employee = current_company.employees.find(params[:id]) if params[:id].present?
    end

    def employee_params
      params.permit(:name, :joining_date, :birth_date)
    end

    def employee_params_present?
      (params[:name].present? && params[:joining_date].present? && params[:birth_date].present?)
    end
  end
end