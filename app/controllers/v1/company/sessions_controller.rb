class V1::Company::SessionsController < ApplicationController
    skip_before_action :authenticated, only: [:create]
    def create
      @company = valid_login?(params[:email_name]) if params[:email_name]
      if  @company && @company.authenticate(params[:password])
        @token = @company.tokens.create  
        render_valid data: {token: TokenSerializer.new(@token)}, message: I18n.t('session.login') 
      end
    end

    def destroy
      token_destroyed = Token.find_by_token get_token         
      render_valid message: 'Company loggrd out successfully'
    end
end