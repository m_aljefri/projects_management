class V1::Company::CompaniesController < ApplicationController
  skip_before_action :authenticated, only: :create

  # get v1/companies/
  def show
    render_valid data: {company: CompanySerializer.new(current_company)}
  end

  # post v1/companies/
  def create
    company = ::Company.create! create_company_params
    token = company.tokens.create! if company.save!
    render_valid data: {token: TokenSerializer.new(token)}, status: :created
  end

  # put /v1/companies/
  def update
    if is_change_password_params_present? 
      change_password change_password_params 
    else edit_profile end
  end

  # delete /v1/companies/
  def destroy
    current_company.destroy
  end








  private

  #----------------------params permit------------------------#
  def change_profile_params
    params.permit(:email, :company_name, :logo, :user_name)
  end
  def change_password_params
    params.permit(:password_confirmation, :password)
  end
  def create_company_params
    params.permit(:email, :company_name, :password, :logo, :user_name, :password_confirmation)    
  end
  #-----------------------------------------------------------#


  def change_password old_password
    current_company.update! change_password_params
    render_valid message: 'Change done successfully'        
  end
  def edit_profile
    update_company change_profile_params
  end
  def update_company params
    current_company.update_attributes! params
    render_valid data: {company: CompanySerializer.new(current_company) }, message: 'Update profile done successfully'
  end

  def is_change_password_params_present?
    ((params[:password])&&(params[:password_confirmation]))
  end
end
