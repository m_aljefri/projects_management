class Employee < ApplicationRecord
    belongs_to :company
    has_many :tasks, foreign_key: 'assignee_id', dependent: :nullify 
    has_many :employees_projects
    has_many :projects, through: :employees_projects, before_remove: :clear_tasks


    #-----------------------validation------------------------#
    validates_presence_of :name, :joining_date, :birth_date
    validate :column_with_date_are_valid_datetime,
             :age_greater_than?
    validates_uniqueness_of :name
    #---------------------------------------------------------#

    #-----------------------callback--------------------------#
    before_destroy :convert_task_to_pending!, prepend: true
    #---------------------------------------------------------#

    #------------------------custom validation methods--------------------------#
    def column_with_date_are_valid_datetime
      errors.add(:birth_date,   'must be a valid datetime') if ((Date.parse(birth_date.to_s)   rescue ArgumentError) == ArgumentError)
      errors.add(:joining_date, 'must be a valid datetime') if ((Date.parse(joining_date.to_s) rescue ArgumentError) == ArgumentError)
    end
    def age_greater_than?
      age = joining_date.year - birth_date.year - ((joining_date.month > birth_date.month || (joining_date.month == birth_date.month && joining_date.day >= birth_date.day)) ? 0 : 1)
      errors.add(:birth_date, 'is less than the 20 year') if age < 20            
    end
    #----------------------------------------------------------------------------#

    #------------------------callback methods-------------------------#
    def convert_task_to_pending!
      task = Task.find_by_assignee_id(self.id)
        if task
          if task.status == 'Complated' 
            task.status = 'Pending'
            task.save 
          end
        end
    end
    #------------------------------------------------------------------#
    

    #---------------------------relation callbacks---------------------------# 
    def clear_tasks(project)
      self.tasks.where(project_id: project.id).update_all(assignee_id: nil, status: 'Pending')
    end
    #-------------------------------------------------------------------------#
end
