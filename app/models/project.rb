class Project < ApplicationRecord
    # include ActiveModel::AttributeAssignment
    belongs_to :company
    has_many   :tasks, dependent: :destroy
    has_many :employees_projects
    has_many :employees, through: :employees_projects, before_remove: :clear_tasks

    #---------------------------relation callbacks--------------------------#
    def clear_tasks(employee)
      employee.tasks.where(project_id: self.id).delete_all
    end
    #-----------------------------------------------------------------------#
end
