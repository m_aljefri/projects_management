class Token < ApplicationRecord
  belongs_to :company
  has_secure_token
end
