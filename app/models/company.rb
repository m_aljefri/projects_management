class Company < ApplicationRecord
    include ValidationHelper  #Concerns/validation_helper.rb

    has_secure_password  
    #--------------relation----------------#
    has_many :employees, dependent: :destroy
    has_many :projects,  dependent: :destroy
    has_many :tokens,    dependent: :destroy
    #--------------------------------------#

    #--------------callbacks---------------#
    before_save  :down_case_email
    after_create :send_wellcome_mail
    after_update :send_reset_password_mail,  if: :reset_password
    after_update :send_change_password_mail, if: :is_changed_password?
    #--------------------------------------#

    
    #--------------mailing companies----------------#
    def send_wellcome_mail
        V1::Notifications.wellcome_email(self).deliver_later
    end
    def send_reset_password_mail
        SendNotificationJob.perform_later(self, 'reset') 
    end   
    def send_change_password_mail
        SendNotificationJob.perform_later(self, 'change') 
    end  
    #------------------------------------------------#
    
    #-------------boolean check to send change password mail---------------#
    # true if password changed from logged user 
    # fals if password changed from reset password
    def is_changed_password?
        self.saved_change_to_password_digest? && !self.saved_change_to_reset_password?
    end
    def down_case_email
        self.email = self.email.downcase
    end
    #----------------------------------------------------------------------#
end
