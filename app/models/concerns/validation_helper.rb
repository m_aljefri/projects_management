module ValidationHelper 
    extend ActiveSupport::Concern
    included do 
      EMAIL_REG = /\A([\w+\-].?)+@[a-zA-Z\d\-]+(\.[a-zA-Z]+)*\.[a-zA-Z]{2,3}\z/
      NAME_REG  = /\A\w+\d{0,}\z/
      validates :email, format: {
                          with: EMAIL_REG
                        }
        
        validates :user_name, format: {
                               with: NAME_REG,
                              }
        validates :password, length: { in: 8..72 }, on: :update, if: :password
        validates_presence_of :company_name, :user_name, :email
        validates_uniqueness_of :company_name, :user_name, :email,  case_sensitive: false
        
        has_attached_file :logo, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
        validates_attachment :logo, presence: true,
        content_type: { content_type: "image/jpeg", message: 'Only image with jpeg/jpg type are valid'},
        size: { in: 0..10.megabytes, message: 'your image size is bigger than possible, please uploade image less than 10 migabytes' }
    end  
end
