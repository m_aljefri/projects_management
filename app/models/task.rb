class Task < ApplicationRecord
    belongs_to :project
    belongs_to :employee , foreign_key: 'assignee_id', optional: true
    

    #-------------------------scopes--------------------------#
    scope :complated!, -> { where(status: "Complated")}
    scope :pendenig!,  -> { where(status: "Pending")}
    scope :assigned!,  -> { where.not(assignee_id: nil)}
    #---------------------------------------------------------#
    
    #------------------------validate-------------------------#
    validate :is_employee_assigned_to_project

    validates :status,
              inclusion: { in: %w(Complated Pending),
              message: "%{value} is not a valid status" }, 
              if: :assignee_id

    validates :status, 
              inclusion: { in: %w(Pending),
              message: "Only Pending valid without assignee" },
              unless: :assignee_id
    #----------------------------------------------------------#

    def is_employee_assigned_to_project
      project = self.project
      employee_ids = project.employees.map(&:id)
      errors.add(:assignee_id,'Should be assignrd to project firstly') unless employee_ids.include?(assignee_id) if assignee_id
    end
end