class TaskSerializer < ApplicationSerializer
  # Serializer for project controller
  attributes :id, :name, :description, :status, :assignee

  def assignee
    if object.assignee_id.present?
      employee = Employee.find(object.assignee_id)
      employee.name
    else
      return nil
    end
  end
end