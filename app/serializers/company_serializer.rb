class CompanySerializer < ApplicationSerializer
  attributes :id, :user_name, :company_name, :email, :logo

  def logo
    {
      medium:   object.logo.url('medium'), 
      thumb:    object.logo.url('thumb'),
      original: object.logo.url('original')
    }
  end
end
