class ProjectSerializer < ApplicationSerializer
    attributes :id, :name, :description
end