class Show::ProjectSerializer < ApplicationSerializer
    # Serializer for show action in project Controller
    attributes :id, :name, :description, :tasks_count, :complated_tasks, :assigned_tasks

    def tasks_count
        object.tasks.count        
    end

    def complated_tasks
        object.tasks.complated!.count
    end

    def assigned_tasks
        object.tasks.assigned!.count     
    end

    # if you cannot do greate things.
    #     do small things in greate way.
    # end  
end