module Show
  class EmployeeSerializer < ApplicationSerializer
    attributes :id, :name, :tasks_count, :projects_count

    has_many :tasks

    def tasks_count
        object.tasks.count
    end
    
    def projects_count
        object.projects.count
    end
  end
end