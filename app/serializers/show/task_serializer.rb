class TaskSerializer < ApplicationSerializer
    # Serializer for employee controller 
    attributes :id, :name, :status, :project_name
    
    def project_name
        project = Project.find(object.id)
        project.name        
    end
end