class TokenSerializer < ApplicationSerializer
  attributes :token

  has_one :company
end
