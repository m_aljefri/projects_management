class EmployeeSerializer < ApplicationSerializer
    attributes :id, :name, :birth_date, :joining_date
end