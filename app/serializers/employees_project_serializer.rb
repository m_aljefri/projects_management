class  EmployeesProjectSerializer < ApplicationSerializer
  attributes :id, :name, :assigned?

  def assigned?
    # list of projects or employees ids for condition below
    if scope[:employee_projects].include?(object.id)
      true
    else
      false
    end
  end
end