class V1::Notifications < ApplicationMailer
    default from: 'project_management@company.com'

    def wellcome_email company
      @company = company
      mail(to: @company.email,
           subject: "Wellcome #{@company.user_name} to Project Management")
    end

    def password_email company
      @url = ActionMailer::Base.default_url_options[:host]
      @company = company
      mail(to: @company.email,
           subject: 'Reset password number')
    end
    def change_password_email company
      @company = company
      mail(to: @company.email,
           subject: 'Change passowrd done successfully')
    end
end
